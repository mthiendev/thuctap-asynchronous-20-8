const StudentApi = {
  getAll() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          data: [
            { id: 1, name: 'Alice' },
            { id: 2, name: 'Bob' },
          ],
          pagination: {
            _total: 2,
            _page: 1,
            _limit: 10,
          },
        });
      }, 1000);
    });
  },
  getAllCallback() {},
};
//Promise
const getStudentsFromAPIPromise = () => {
  StudentApi.getAll()
    .then((result) => {
      console.log('getStudentsFromAPIPromise', result.data);
    })
    .catch((err) => console.log(err));
};
getStudentsFromAPIPromise();
// Asyn/Await
const getStudentsFromAPIAsynAwait = async () => {
  try {
    const result = await StudentApi.getAll();
    console.log('getStudentsFromAPIAsynAwait: ', result.data);
  } catch (error) {
    console.log(error);
  }
};
getStudentsFromAPIAsynAwait();
// callback
function getName(name, callback) {
  setTimeout(() => {
    console.log(name);
    callback();
  }, Math.floor(Math.random() * 100) + 1);
}
function getAll() {
  getName('JS', () => {
    getName('is', () => {
      getName('easy :))', () => {});
    });
  });
}
getAll();
